﻿# Open source "Dragonica Mercy"

This is the free source code of the private server "Dragonica Mercy".

Published for review, all rights to the game belong to Gravity Interactive Inc.

If you want to use parts of the source code or full source code on your project, you must notify the author
of the source code by email: ShivaCloud@yandex.ru.
Also specify a link to this repository on the page of your project.

[Official Discord Community](https://discord.gg/GjCRtrqqxu)

## Project struct

In order for everything to work as the author intended, your project structure should look like this

```
.
├── database
├── dragonica_exe
│   ├── ToolBin
│   ├── archangel_run
│   └── sfreedom_dev
└── dragonica_master
```

### Description 

| Folder | Description|
| ------------- |-------------|
| ToolBin | Built Dragonica tools |
| archangel_run | Server files |
| sfreedom_dev | Client files |
| dragonica_master | Source code |
| database | database Backup |

## Installation

### Client, server and source code downloads

> The repository size is too large, it would be better if you downloaded it from [Yandex disk](https://disk.yandex.ru/d/Yhi0L4XpyDyRpg)
> and then update

1. **To begin**, we need to install [git](https://git-scm.com/downloads) in order to download files.
2. **Once git has been successfully installed**, *navigate to the folder* where you want to download files.
3. *Right-click* on an empty space inside this folder and select the option `Git Bash Here`.
   - If you are using Windows 11, click on `Show More option` first.
4. **Copy and paste** the following commands into the opened window:
    ```sh
    git clone https://gitlab.com/open_dragonica1/project_d.git
    cd project_d
    git submodule update --init --recursive --progress
    ```
5. *Wait for the download* to complete.

## Update

1. **To begin**, we need to install [git](https://git-scm.com/downloads) in order to download files.
2. **Once git has been successfully installed**, *navigate to the folder* where you want to download files.
3. *Right-click* on an empty space inside `project_d` folder and select the option `Git Bash Here`.
   - If you are using Windows 11, click on `Show More option` first.
4. **Copy and paste** the following commands into the opened window:
    ```sh
    git pull
    git submodule update --init --recursive --progress
    ```
5. *Wait for the download* to complete.

### Setting up the server environment

1. **To begin**, you need to download and install [SQL Server Express](https://www.microsoft.com/en-us/sql-server/sql-server-downloads).
2. **Next**, download and install [Python 3.11](https://www.python.org/downloads/). We will need it for server configuration. Set check box `Add python.exe to PATH`, that very important!
3. Install [SQL Server Management Studio](https://learn.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver16#download-ssms)
4. Launch SQL Server Management Studio.

5. You should see a window that looks like this:
   > ![Auth](./images/tutorial/sql01.png)

6. Click the "Connect" button.
7. After successfully connecting, we need to restore the databases.

   1. Manual restoring:
    > ![Auth](./images/tutorial/sql03.gif)
    >
    > ![Auth](./images/tutorial/sql04.gif)
   2. If you have trouble with DB restore then do this extra steep
    > ![Auth](./images/tutorial/sql08.png)
8. Let's activate the `sa` user and set the password to `123456`.
   1. By script
        1. Click "New Query"
            > ![Auth](./images/tutorial/sql02.gif)
        2. In the query window that appears, paste the following script:
            ```sql
            USE [master]
            EXEC xp_instance_regwrite N'HKEY_LOCAL_MACHINE', N'Software\Microsoft\MSSQLServer\MSSQLServer', N'LoginMode', REG_DWORD, 2;
            ALTER LOGIN sa WITH PASSWORD = '123456';
            ALTER LOGIN sa ENABLE;
            GO
            ```
        3.  Press Execute or `F5`
        4.  Restart the SQL Server service to apply the changes.
            > ![Auth](./images/tutorial/sql05.png)
    2. Manual
        1. In SQL Server Management Studio, expand the "Security" folder in the Object Explorer.
        2. Locate and right-click on the "sa" login under the "Logins" folder.
        3. Select "Properties" to open the "Login Properties" window.
        4. In the "General" tab, you'll find the "Password" section. Enter the new password, which in this case is "123456", in both the "Password" and "Confirm password" fields.
        5. Ensure that "Enforce password policy" and "Enforce password expiration" options are unchecked to prevent any conflicting policies.
        6. Switch to the "Status" tab, and under the "Login" section, select "Enabled" to activate the `sa` user account.
        7. Click "OK" to save the changes and apply the new password and enable the `sa` user.
        8. In the "Server Properties" window, go to the "Security" page.
        9. Under the "Server authentication" section, select the "SQL Server and Windows Authentication mode" option.
        10. Click "OK" to save the changes.
        11. Restart the SQL Server service to apply the changes.
            > ![Auth](./images/tutorial/sql05.png)
9. MSSQL configuration is complete!
10. Now, we need to configure the server.
11. Open the folder `dragonica_Exe\archangel_run`.
12. *Right-click* on an empty space inside this folder and select the option `Git Bash Here`.
   - If you are using Windows 11, click on `Show More option` first.
13. Copy and paste the following commands into the opened window, after replacing `SERVER_NAME\\SQLEXPRESS` with your actual SQL Server name, which you can find in the "Connect" window.
Use `\\` instead of `\`!
    ```sh
    cd Tools
    python.exe db_cfg_gen.py --id sa --pw 123456 --addr SERVER_NAME\\SQLEXPRESS
    ```
1.  The basic server configuration is complete!
2.  Next, run MMC.bat, SMC.bat, and ServerControl_Release.exe.
3.  The login credentials for accessing the server admin panel are:
    Username: 1
    Password: 1
4.  After starting all servers in the admin panel, you can log in using the client located in `dragonica_Exe\sfreedom_dev`.
5.  To start the client, use `dragonica_local.cmd`.
6.  The login credentials for the game are:
    Username: 1
    Password: 1


> Feel free to join our [Discord channel](https://discord.gg/GjCRtrqqxu) and ask any questions you may have! 

### Setting up the environment for working with the source code

1. Download and install [Visual Studio 2022 Community](https://visualstudio.microsoft.com/vs/).
2. Install Desktop development with C++ component from Workloads tab
3. Install the following individual components:
   - C++ CMake tools for Windows
   - MSVC v143 2022 C++ x64/x86 build tools (Latest)
   - C++ ATL for latest v143 build tools (x86 & x64)
   - C++ MFC for latest v143 build tools (x86 & x64)
   - Windows 10 SDK 10.0.19041.0
   - Microsoft Foundation Classes for C++
4. Open Visual Studio 2022.
5. Select "Open Folder" from the start page.
6. Open the folder `dragonica_Exe\dragonica_master`.
7. Wait for the CMake project configuration to complete.

After completing these steps, your environment will be set up for working with the source code. You can start working on the project using Visual Studio 2022.

> Feel free to join our [Discord channel](https://discord.gg/GjCRtrqqxu) and ask any questions you may have! 

## Visual studio project description

### Configuration

1. **Develop** - The "dev" configuration is intended for internal testing purposes only. The client contains GM (Game Master) commands that are useful for development and testing tasks.
2. **Release** - The "production" configuration is suitable for running a private server in a production environment. If you are setting up your own private server, it is recommended to use this configuration for stability and security reasons.

You can choose the configuration in Visual Studio by following these steps:

1. Click on the drop-down menu in the top toolbar, as shown in the image below:
   ![MSVC](./images/tutorial/msvc01.gif)
2. Select the desired configuration from the list, such as "Develop" or "Release," based on your requirements.

To build the project, please follow these instructions:

1. Click on the "Switch between solutions and available views" menu in Visual Studio, then choose "CMake Targets View" as shown below:
   ![MSVC](./images/tutorial/msvc02.gif)

After switching to the "CMake Targets View," you will have access to various CMake targets and build options.

You can build the project by selecting the appropriate target and clicking on the build button.

The Cosmos source code can be found in the `dragonica_master/cosmos/include` directory.

> Feel free to join our [Discord channel](https://discord.gg/GjCRtrqqxu) and ask any questions you may have! 

## Features

`Private` is an internal version that can be obtained upon request by messaging "reoil" on Discord.

### Game

| Feature                              | Open |  Private |
| ------------------------------------ | :--: |  :-----: |
| Costume system                       |  ✅  |    ✅   |
| Mutators                             |  ✅  |    ✅   |
| Borderless window                    |  ✅  |    ✅   |
| Discord rich services                |  ✅  |    ✅   |
| Daily reward                         |  ✅  |    ✅   |
| Multi-Language support (RU/EN/FR)    |  ✅  |    ✅   |
| Escort mode mission                  |  ✅  |    ✅   |
| Freeze player instead of ban         |  ✅  |    ✅   |
| Unfreeze player by quest             |  ✅  |    ✅   |
| Fix client crash after duel          |  ✅  |    ✅   |
| BSQ reward for lose                  |  ✅  |    ✅   |
| User spam to chat exploit fix        |  ✅  |    ✅   |
| Empty username exploit fix           |  ✅  |    ✅   |
| Create drakan by special item        |  ✅  |    ✅   |
| Display exp percent in quest         |  ✅  |    ✅   |
| Save trade chat on server reload     |  ✅  |    ✅   |
| Weapon Kill track                    |  ✅  |    ✅   |
| Inventory second page                |  ✅  |    ✅   |
| Inventory indicator in UI            |  ✅  |    ✅   |
| Night mode                           |  ✅  |    ✅   |
| New encryption                       |  ❌  |    ✅   |
| World PVP                            |  ❌  |    ✅   |
| Cash dupe fix                        |  ❌  |    ✅   |
| Gold dupe fix                        |  ❌  |    ✅   |
| Element dupe fix                     |  ❌  |    ✅   |
| Market dupe fix                      |  ❌  |    ✅   |
| Scroll dupe fix                      |  ❌  |    ✅   |
| Soul stone dupe fix                  |  ❌  |    ✅   |
| Easy Anti Cheat                      |  ❌  |    ✅   |


### Developer

| Feature                              | Open | Private |
| ------------------------------------ | :--: | :-----: |
| UI hot reload                        |  ✅  |    ✅   |
| SMC server crash notify to telegram  |  ✅  |    ✅   |
| CMake build system                   |  ✅  |    ✅   |
| CSV table instead of MSSQL           |  ✅  |    ✅   |
| Source converted to UTF-8            |  ✅  |    ✅   |
| GM API for python                    |  ✅  |    ✅   |
| Item Hot reload                      |  ✅  |    ✅   |
| Debug info for items /SetDisplayAbilNo(1) |  ✅  |    ✅   |
| Skill info debug /SetSkillDebugDialog(1) |  ✅  |    ✅   |
| Free camera /ToggleFreeCam()         |  ✅  |    ✅   |
| Game pause /Net_TogglePause()        |  ✅  |    ✅   |
| Gitlab CI                            | ❌  |    ✅   |
| Gamebryo Source code                 | ❌  |    ✅   |
| New GM Tool Source code              | ❌  |    ✅   |
| Patch maker tools                    | ❌  |    ✅   |

> To discover more about the features, please join our official [Discord channel](https://discord.gg/GjCRtrqqxu).

> Come and be a part of our community, where you'll find detailed discussions and insights about the mentioned features and much more!
